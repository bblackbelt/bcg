package com.blackbelt.bcgcodechallenge

import android.support.annotation.NonNull
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class RxTestRule : TestRule {

    private var mScheduler: Scheduler = object : Scheduler() {

        override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
            // this prevents StackOverflowErrors when scheduling with a delay
            return super.scheduleDirect(run, 0, unit)
        }

        override fun createWorker(): Scheduler.Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }

    constructor() {
        mScheduler = Schedulers.trampoline()
    }

    constructor(scheduler: Scheduler) {
        mScheduler = scheduler
    }

    fun updateComputationScheduler(newScheduler: Scheduler) {
        RxJavaPlugins.setComputationSchedulerHandler { scheduler -> newScheduler }
    }

    fun resetSchedulers() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

    override fun apply(base: Statement, description: Description): Statement {

        return object : Statement() {

            override fun evaluate() {
                RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> mScheduler }
                RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> mScheduler }

                try {
                    base.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
    }
}