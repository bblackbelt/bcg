package com.blackbelt.bcgcodechallenge.view.main.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.blackbelt.bcgcodechallenge.RxTestRule
import com.blackbelt.bcgcodechallenge.data.model.Avatar
import com.blackbelt.bcgcodechallenge.domain.user.IUserManager
import com.blackbelt.bcgcodechallenge.domain.user.model.User
import io.reactivex.Observable
import org.junit.Assert
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.io.File
import java.io.IOException


@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {
    companion object {

        @ClassRule
        @JvmField
        val instantRule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val rxTestRule = RxTestRule()
    }

    @org.junit.Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }


    @Mock
    private lateinit var mUserManager: IUserManager

    private val mMainViewModel: MainViewModel by lazy {
        MainViewModel(mUserManager)
    }

    @Test
    fun test_get_user() {

        val user = User("test@test.com", "avatarUrl")

        Mockito.`when`(mUserManager.getLoggedInUser())
                .thenReturn(Observable.just(user))

        mMainViewModel.loadUser()

        Assert.assertTrue(mMainViewModel.avatarUrl == user.avatarUrl)
        Assert.assertTrue(mMainViewModel.email == user.email)

    }

    @Test
    fun test_get_user_failed() {

        val exception = IOException()

        Mockito.`when`(mUserManager.getLoggedInUser())
                .thenReturn(Observable.error(exception))

        val viewModel = Mockito.spy(MainViewModel(mUserManager))

        viewModel.loadUser()

        Assert.assertTrue(viewModel.avatarUrl.isNullOrEmpty())
        Assert.assertTrue(viewModel.email.isNullOrEmpty())

        Mockito.verify(viewModel).handlerError(exception)

        viewModel.getErrorText()?.let {
            Assert.assertTrue(it > 0)
        }

        Assert.assertTrue(viewModel.isErrorViewVisible())
    }

    @Test
    fun test_reload_user_reset_error() {

        val exception = IOException()

        Mockito.`when`(mUserManager.getLoggedInUser())
                .thenReturn(Observable.error(exception))

        val viewModel = Mockito.spy(MainViewModel(mUserManager))

        viewModel.loadUser()

        Mockito.verify(viewModel).handlerError(exception)

        viewModel.getErrorText()?.let {
            Assert.assertTrue(it > 0)
        }

        Assert.assertTrue(viewModel.isErrorViewVisible())

        val user = User("test@test.com", "avatarUrl")

        Mockito.`when`(mUserManager.getLoggedInUser())
                .thenReturn(Observable.just(user))

        viewModel.reload()

        viewModel.getErrorText()?.let {
            Assert.assertTrue(it == 0)
        }

        Assert.assertFalse(viewModel.isErrorViewVisible())
    }

    @Test
    fun test_upload_picture() {

        val avatar = Avatar("newPicture")
        val pictureFile = File("")

        Mockito.`when`(mUserManager.updateAvatar(pictureFile))
                .thenReturn(Observable.just(avatar))

        val viewModel = Mockito.spy(MainViewModel(mUserManager))

        viewModel.avatarUrl = "oldPicture"

        viewModel.onNewPicture(pictureFile)

        Assert.assertTrue(avatar.avatarUrl == viewModel.avatarUrl)
    }

    @Test
    fun test_upload_failed() {

        val pictureFile = File("")
        val exception = IOException()

        Mockito.`when`(mUserManager.updateAvatar(pictureFile))
                .thenReturn(Observable.error(exception))

        val viewModel = Mockito.spy(MainViewModel(mUserManager))

        viewModel.avatarUrl = "oldPicture"

        viewModel.onNewPicture(pictureFile)

        Assert.assertTrue("oldPicture" == viewModel.avatarUrl)

        Mockito.verify(viewModel).uploadPictureFailed(exception)
    }
}