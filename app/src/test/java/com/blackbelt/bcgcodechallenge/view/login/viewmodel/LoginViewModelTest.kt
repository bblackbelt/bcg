package com.blackbelt.bcgcodechallenge.view.login.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.blackbelt.bcgcodechallenge.R
import com.blackbelt.bcgcodechallenge.RxTestRule
import com.blackbelt.bcgcodechallenge.domain.user.IUserManager
import com.blackbelt.bcgcodechallenge.domain.user.model.Login
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException


@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    companion object {

        @ClassRule
        @JvmField
        val instantRule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val rxTestRule = RxTestRule()
    }

    @Mock
    private lateinit var mUserManager: IUserManager

    private val mLoginViewModel: LoginViewModel by lazy {
        LoginViewModel(mUserManager)
    }

    @org.junit.Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun test_invalid_email() {
        mLoginViewModel.email = "aa"
        Assert.assertFalse(mLoginViewModel.isValidEmail(mLoginViewModel.email))
    }

    @Test
    fun test_empty_email() {
        mLoginViewModel.email = ""
        Assert.assertFalse(mLoginViewModel.isValidEmail(mLoginViewModel.email))
    }

    @Test
    fun test_valid_email() {
        mLoginViewModel.email = "test@test.com"
        Assert.assertTrue(mLoginViewModel.isValidEmail(mLoginViewModel.email))
    }

    @Test
    fun test_valid_password() {
        mLoginViewModel.password = "123456"
        Assert.assertTrue(mLoginViewModel.isValidPassword(mLoginViewModel.password))
    }

    @Test
    fun test_cant_login_email() {
        mLoginViewModel.email = "aa"
        mLoginViewModel.password = "123456"
        Assert.assertFalse(mLoginViewModel.canLogin())
    }

    @Test
    fun test_can_login() {
        mLoginViewModel.email = "test@test.com"
        mLoginViewModel.password = "123456"
        Assert.assertTrue(mLoginViewModel.canLogin())
    }

    @Test
    fun test_login_successful() {

        val login = Login("test@test.com", "usertoken")

        Mockito.`when`(mUserManager.login(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Observable.just(login))

        val loginViewModel = LoginViewModel(mUserManager)
        val spyViewModel = Mockito.spy(loginViewModel)

        Mockito.doReturn(true).`when`(spyViewModel).canLogin()

        spyViewModel.doLogin()
        Mockito.verify(spyViewModel, times(1)).handleLoginSuccessful(login)
    }

    @Test
    fun test_login_network_error() {

        val ioException = IOException()

        Mockito.`when`(mUserManager.login(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Observable.error(ioException))

        val loginViewModel = LoginViewModel(mUserManager)
        val spyViewModel = Mockito.spy(loginViewModel)

        Mockito.doReturn(true).`when`(spyViewModel).canLogin()

        spyViewModel.doLogin()
        Mockito.verify(spyViewModel, times(1)).handlerError(ioException)
        Assert.assertTrue(spyViewModel.getErrorString(ioException) == R.string.connection_error)
    }

    @Test
    fun test_login_401() {

        val error = Response.error<Any>(401, ResponseBody
                .create(MediaType.parse("application/json"), "{}"))

        val ioException = HttpException(error)

        Mockito.`when`(mUserManager.login(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Observable.error(ioException))

        val loginViewModel = LoginViewModel(mUserManager)
        val spyViewModel = Mockito.spy(loginViewModel)

        Mockito.doReturn(true).`when`(spyViewModel).canLogin()

        spyViewModel.doLogin()
        Mockito.verify(spyViewModel, times(1)).handlerError(ioException)
        Assert.assertTrue(spyViewModel.getErrorString(ioException) == R.string.wrong_credentials)
    }
}