package com.blackbelt.bcgcodechallenge.domain.user

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import com.blackbelt.bcgcodechallenge.data.IUserDataRepository
import com.blackbelt.bcgcodechallenge.data.PreferencesRepository
import com.blackbelt.bcgcodechallenge.data.model.Avatar
import com.blackbelt.bcgcodechallenge.data.model.AvatarRequest
import com.blackbelt.bcgcodechallenge.data.model.LoginRequest
import com.blackbelt.bcgcodechallenge.domain.user.model.Login
import com.blackbelt.bcgcodechallenge.domain.user.model.User
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.ByteArrayOutputStream
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

interface IUserManager {
    fun login(email: String, password: String): Observable<Login>
    fun isLoggedIn(): Boolean
    fun getLoggedInUser(): Observable<User>
    fun updateAvatar(pictureUrl: File): Observable<Avatar>
}

@Singleton
class UserManager @Inject constructor(userDataRepository: IUserDataRepository, preferencesRepository: PreferencesRepository) : IUserManager {

    private val mDataRepository = userDataRepository
    private val mPreferencesRepository = preferencesRepository

    override fun login(email: String, password: String): Observable<Login> {
        return mDataRepository.login(LoginRequest(email, password))
                .map { it ->
                    mPreferencesRepository.saveUserId(it.userId ?: "")
                    mPreferencesRepository.saveToken(it.token ?: "")
                    Login(it.userId, it.token)
                }
    }

    override fun getLoggedInUser(): Observable<User> {
        val token = mPreferencesRepository.getToken()
        val userId = mPreferencesRepository.getUserId()
        return mDataRepository.getUser(token, userId)
                .map { User(it.email, it.avatarUrl) }
    }

    override fun updateAvatar(pictureUrl: File): Observable<Avatar> {
        return Observable.just(pictureUrl)
                .subscribeOn(Schedulers.computation())
                .map {
                    val bitmapOptions = BitmapFactory.Options()
                    bitmapOptions.inJustDecodeBounds = true
                    BitmapFactory.decodeFile(it.absolutePath, bitmapOptions)

                    val reqHeight = if (bitmapOptions.outHeight > bitmapOptions.outWidth) {
                        640
                    } else {
                        480
                    }

                    val reqWidth = if (bitmapOptions.outHeight > bitmapOptions.outWidth) {
                        480
                    } else {
                        640
                    }

                    var inSampleSize = 1
                    if (bitmapOptions.outHeight > reqHeight || bitmapOptions.outWidth > reqWidth) {

                        val halfHeight = bitmapOptions.outHeight / 2
                        val halfWidth = bitmapOptions.outWidth / 2

                        while ((halfHeight / inSampleSize) >= reqHeight
                                && (halfWidth / inSampleSize) >= reqWidth) {
                            inSampleSize *= 2
                        }
                    }

                    bitmapOptions.inJustDecodeBounds = false
                    bitmapOptions.inSampleSize = inSampleSize
                    val bitmap = BitmapFactory.decodeFile(it.absolutePath, bitmapOptions)

                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    val byteArray = byteArrayOutputStream.toByteArray()
                    Base64.encodeToString(byteArray, Base64.DEFAULT)
                }.flatMap {
                    mDataRepository.uploadAvatar(mPreferencesRepository.getToken(),
                            mPreferencesRepository.getUserId(),
                            AvatarRequest(it))
                            .map {
                                Avatar(it.avatarUrl)
                            }
                }

    }


    override fun isLoggedIn(): Boolean = mPreferencesRepository.getToken().isNotEmpty()
}