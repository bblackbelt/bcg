package com.blackbelt.bcgcodechallenge.domain.user.model

data class Login(val userId: String? = null, val token: String? = null)