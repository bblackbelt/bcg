package com.blackbelt.bcgcodechallenge.domain.user.model

data class User(val email: String? = null, val avatarUrl: String? = null)