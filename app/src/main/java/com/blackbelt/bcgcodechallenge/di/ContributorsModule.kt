package com.blackbelt.bcgcodechallenge.di

import com.blackbelt.bcgcodechallenge.view.main.MainActivity
import com.blackbelt.bcgcodechallenge.view.login.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ContributorsModule {

    @ContributesAndroidInjector
    fun injectLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    fun injectMainActivity(): MainActivity
}