package com.blackbelt.bcgcodechallenge.di

import android.content.Context
import com.blackbelt.bcgcodechallenge.data.MockUserDataService
import com.blackbelt.bcgcodechallenge.data.UserDataService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.io.File
import javax.inject.Singleton


private const val HTTP_RESPONSE_DISK_CACHE_MAX_SIZE = (5 * 1024 * 1024).toLong()

@Module
class NetworkModule {

    @Provides
    fun provideCacheFile(context: Context): Cache {
        val baseDir = context.cacheDir
        val cacheDir = File(baseDir, "HttpResponseCache")
        return Cache(cacheDir, HTTP_RESPONSE_DISK_CACHE_MAX_SIZE)
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun providesRxJava2CallAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

    @Provides
    @Singleton
    fun provideUserDataService(context: Context, gson: GsonConverterFactory,
                               rxJava2CallAdapterFactory: RxJava2CallAdapterFactory): UserDataService {
        val builder = Retrofit.Builder()
                .baseUrl("http://localhost")
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gson)

        val retrofit = builder.build()

        val behavior = NetworkBehavior.create()
        behavior.setFailurePercent(20)

        val mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build()

        val delegate = mockRetrofit.create(UserDataService::class.java)
        return MockUserDataService(delegate, File(context.getExternalFilesDir(null), "webServerCache"))
    }
}