package com.blackbelt.bcgcodechallenge.di

import android.content.Context
import com.blackbelt.bcgcodechallenge.App
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class HelpersModule {

    @Provides
    fun provideContext(app: App): Context = app.applicationContext

    @Provides
    fun provideResources(context: Context) = context.resources

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun providesSharedPreferences(context: Context) = context.getSharedPreferences("PREFS", Context.MODE_PRIVATE)


}