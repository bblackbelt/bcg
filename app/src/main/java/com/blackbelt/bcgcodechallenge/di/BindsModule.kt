package com.blackbelt.bcgcodechallenge.di

import com.blackbelt.bcgcodechallenge.data.IPreferencesRepository
import com.blackbelt.bcgcodechallenge.data.IUserDataRepository
import com.blackbelt.bcgcodechallenge.data.PreferencesRepository
import com.blackbelt.bcgcodechallenge.data.UserDataRepository
import com.blackbelt.bcgcodechallenge.domain.user.IUserManager
import com.blackbelt.bcgcodechallenge.domain.user.UserManager
import dagger.Binds
import dagger.Module

@Module
abstract class BindsModule {

    @Binds
    abstract fun bindUserDataRepository(userDataRepository: UserDataRepository): IUserDataRepository

    @Binds
    abstract fun bindUserManager(userManager: UserManager): IUserManager

    @Binds
    abstract fun bindPreferencesRepository(preferencesRepository: PreferencesRepository): IPreferencesRepository
}