package com.blackbelt.bcgcodechallenge.view.utils

import android.databinding.BindingAdapter
import android.graphics.BitmapFactory
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.widget.ImageView
import android.widget.TextView
import com.blackbelt.bcgcodechallenge.R
import com.squareup.picasso.Picasso
import java.io.File


@BindingAdapter("srcUrl", "rounded")
fun ImageView.loadImage(url: String?, rounded: Boolean = false) {

    val placeHolder = BitmapFactory.decodeResource(resources, R.drawable.placeholder)
    val roundedPlaceholder = RoundedBitmapDrawableFactory.create(resources, placeHolder)
    roundedPlaceholder.isCircular = true

    if (url == null) {
        setImageDrawable(roundedPlaceholder)
        return
    }

    val picasso = Picasso.Builder(context).listener { _, _, exception -> exception?.printStackTrace() }.build()

    val request = if (url.isNotEmpty()) {
        if (url.startsWith("http") || url.startsWith("https")) {
            picasso.load(url)
        } else {
            picasso.load(File(url))
        }
    } else {
        return
    }

    request.placeholder(roundedPlaceholder)
            .fit()
            .centerCrop()

    if (rounded) {
        request.transform(RoundedTransformation(url))
    }

    request.into(this)
}

@BindingAdapter("toTextColor")
fun TextView.setTextColorChecked(@ColorRes color: Int) {
    try {
        if (color != 0) {
            setTextColor(ContextCompat.getColor(context, color))
        }
    } catch (e: Exception) {
    }
}

@BindingAdapter("toText")
fun TextView.setTextChecked(@StringRes stringRes: Int) {
    try {
        if (stringRes != 0) {
            setText(stringRes)
        }
    } catch (e: Exception) {
    }
}