package com.blackbelt.bcgcodechallenge.view.login.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.Bindable
import com.blackbelt.bcgcodechallenge.BR
import com.blackbelt.bcgcodechallenge.R
import com.blackbelt.bcgcodechallenge.domain.user.IUserManager
import com.blackbelt.bcgcodechallenge.domain.user.model.Login
import com.blackbelt.bindings.notifications.ClickItemWrapper
import com.blackbelt.bindings.notifications.MessageWrapper
import com.blackbelt.bindings.viewmodel.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import retrofit2.HttpException
import java.io.IOException
import java.util.regex.Pattern
import javax.inject.Inject

open class LoginViewModel constructor(userManager: IUserManager) : BaseViewModel() {

    private val mUserManager = userManager

    private val mEmailPattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+")

    var loading: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    var email: String = ""

    var password: String = ""

    var mLoginDisposable = Disposables.disposed()

    @Bindable
    fun getButtonLabel(): Int = R.string.login

    fun doLogin() {
        if (!canLogin()) {
            return
        }
        loading = true
        mLoginDisposable =
                getLoginObservable()
                        .subscribe({
                            handleLoginSuccessful(it)
                        }, { it -> handlerError(it) })
    }

    fun getLoginObservable(): Observable<Login> = mUserManager.login(email, password)
            .observeOn(AndroidSchedulers.mainThread())

    internal open fun handleLoginSuccessful(login: Login) {
        loading = false
        mItemClickNotifier.value = ClickItemWrapper.withAdditionalData(R.id.start_main_activity, login)
    }

    override fun handlerError(throwable: Throwable) {
        super.handlerError(throwable)
        loading = false
        mMessageNotifier.value = MessageWrapper.withSnackBar(getErrorString(throwable))
    }

    internal fun getErrorString(throwable: Throwable) = if (throwable is HttpException) {
        if (throwable.code() == 401) {
            R.string.wrong_credentials
        } else {
            R.string.connection_error
        }
    } else if (throwable is IOException) {
        R.string.connection_error
    } else {
        R.string.oops_something_went_wrong
    }

    open fun canLogin(): Boolean {

        if (!isValidEmail(email)) {
            mMessageNotifier.value = MessageWrapper.withSnackBar(R.string.invalid_email)
            return false
        }

        if (!isValidPassword(password)) {
            mMessageNotifier.value = MessageWrapper.withSnackBar(R.string.invalid_password)
            return false
        }

        return true
    }

    fun isLoggedIn() = mUserManager.isLoggedIn()

    fun isValidPassword(password: String): Boolean {
        return password.isNotEmpty()
    }

    fun isValidEmail(email: String): Boolean {
        return mEmailPattern.matcher(email).matches()
    }

    override fun onDestroy() {
        super.onDestroy()
        mLoginDisposable.dispose()
    }

    class Factory @Inject constructor(userManager: IUserManager) : ViewModelProvider.NewInstanceFactory() {
        private val mUserManager = userManager

        override fun <T : ViewModel?> create(modelClass: Class<T>): T = LoginViewModel(mUserManager) as T
    }
}