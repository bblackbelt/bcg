package com.blackbelt.bcgcodechallenge.view.login

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.blackbelt.bcgcodechallenge.R
import com.blackbelt.bcgcodechallenge.view.login.viewmodel.LoginViewModel
import com.blackbelt.bcgcodechallenge.view.main.MainActivity
import com.blackbelt.bindings.BR
import com.blackbelt.bindings.activity.BaseBindingActivity
import com.blackbelt.bindings.notifications.ClickItemWrapper
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseBindingActivity() {

    @Inject
    lateinit var mFactory: LoginViewModel.Factory

    private val mLoginViewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, mFactory)[LoginViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        if (mLoginViewModel.isLoggedIn()) {
            startMainActivity()
            return
        }

        setContentView(R.layout.activity_login, BR.loginViewModel, mLoginViewModel)
        setSupportActionBar(toolbar)
    }

    override fun onItemClicked(itemWrapper: ClickItemWrapper) {
        super.onItemClicked(itemWrapper)
        startMainActivity()
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}