package com.blackbelt.bcgcodechallenge.view.main

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.View
import com.blackbelt.bcgcodechallenge.BR
import com.blackbelt.bcgcodechallenge.R
import com.blackbelt.bcgcodechallenge.view.main.viewmodel.MainViewModel
import com.blackbelt.bindings.activity.BaseBindingActivity
import com.blackbelt.bindings.notifications.ClickItemWrapper
import dagger.android.AndroidInjection
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.lang.Exception
import javax.inject.Inject


class MainActivity : BaseBindingActivity() {

    @Inject
    lateinit var mFactory: MainViewModel.Factory

    private val mMainViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, mFactory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main, BR.mainViewModel, mMainViewModel)
    }

    private fun showGalleryCameraDialog() {
        val view = layoutInflater.inflate(R.layout.camera_gallery_layout, null)

        val dialog = AlertDialog.Builder(this)
                .setTitle(R.string.select_photo)
                .setView(view)
                .setNegativeButton(android.R.string.cancel, null)
                .create()

        dialog.window.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimaryDark)))

        view.findViewById<View>(R.id.camera).setOnClickListener {
            dialog.dismiss()
            EasyImage.openCamera(this, 0)
        }

        view.findViewById<View>(R.id.gallery).setOnClickListener {
            dialog.dismiss()
            EasyImage.openGallery(this, 0)
        }

        dialog.show()
    }

    override fun onItemClicked(itemWrapper: ClickItemWrapper) {
        super.onItemClicked(itemWrapper)
        showGalleryCameraDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
            }

            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                mMainViewModel.onNewPicture(imageFile)
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@MainActivity)
                    photoFile?.delete()
                }
            }
        })
    }
}
