package com.blackbelt.bcgcodechallenge.view.main.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.Bindable
import com.blackbelt.bcgcodechallenge.BR
import com.blackbelt.bcgcodechallenge.R
import com.blackbelt.bcgcodechallenge.domain.user.IUserManager
import com.blackbelt.bcgcodechallenge.view.IErrorView
import com.blackbelt.bindings.notifications.ClickItemWrapper
import com.blackbelt.bindings.notifications.MessageWrapper
import com.blackbelt.bindings.viewmodel.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import java.io.File
import java.io.IOException
import javax.inject.Inject

open class MainViewModel(userManager: IUserManager) : BaseViewModel(), IErrorView {

    private val mUserManager = userManager

    private var mDisposable = Disposables.disposed()

    private var mUploadPicturesDisposable = Disposables.disposed()

    var avatarUrl: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.avatarUrl)
        }

    var email: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.email)
        }

    var loading: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    var loadingPicture: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.loadingPicture)
        }

    private var mErrorResId: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.errorText)
            notifyPropertyChanged(BR.errorViewVisible)
        }

    override fun onCreate() {
        super.onCreate()
        loadUser()
    }

    internal fun loadUser() {
        loading = true
        mDisposable.dispose()
        mDisposable = mUserManager.getLoggedInUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->
                    avatarUrl = it.avatarUrl
                    email = it.email
                    loading = false
                }, { handlerError(it) })
    }

    override fun handlerError(throwable: Throwable) {
        super.handlerError(throwable)
        loading = false
        mErrorResId = if (throwable is IOException) {
            R.string.connection_error
        } else {
            R.string.oops_something_went_wrong
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
        mUploadPicturesDisposable.dispose()
    }

    fun updatePicture() {
        mItemClickNotifier.value = ClickItemWrapper.simpleClickItemWrapper(0)
    }

    fun onNewPicture(pictureUrl: File?) {
        pictureUrl ?: return

        mUploadPicturesDisposable.dispose()
        loadingPicture = true
        mUploadPicturesDisposable = mUserManager.updateAvatar(pictureUrl)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadingPicture = false
                    avatarUrl = it.avatarUrl
                }, {
                    uploadPictureFailed(it)
                })
    }

    internal open fun uploadPictureFailed(throwable: Throwable) {
        loadingPicture = false
        mMessageNotifier.value = MessageWrapper.withSnackBar(R.string.avatar_upload_failed)
        throwable.printStackTrace()
    }

    @Bindable
    override fun getErrorText(): Int? = mErrorResId

    override fun reload() {
        mErrorResId = 0
        loadUser()
    }

    @Bindable
    override fun isErrorViewVisible(): Boolean = mErrorResId > 0

    class Factory @Inject constructor(userManager: IUserManager) : ViewModelProvider.NewInstanceFactory() {
        private val mUserManager = userManager

        override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel(mUserManager) as T
    }

}