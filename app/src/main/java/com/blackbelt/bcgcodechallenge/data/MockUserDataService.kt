package com.blackbelt.bcgcodechallenge.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import com.blackbelt.bcgcodechallenge.data.model.*
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls
import java.io.File
import java.io.FileOutputStream


class MockUserDataService(delegate: BehaviorDelegate<UserDataService>, mockWebServerCache: File) : UserDataService {

    private val mValidCredentials = LoginRequest("test@test.com", "test123")

    private val mToken = "VERY_SECRET_TOKEN"

    private val mUserId = "THE_USER_ID"

    private val mResponseDelegate = delegate

    private val mMockWebServerCache = mockWebServerCache

    init {
        mMockWebServerCache.mkdirs()
    }

    override fun login(loginRequest: LoginRequest): Observable<LoginEntity> {
        if (mValidCredentials != loginRequest) {
            val json = JSONObject()
            val error = Response.error<Any>(401, ResponseBody
                    .create(MediaType.parse("application/json"), json.toString()))
            return mResponseDelegate.returning(Calls.response(error)).login(loginRequest)
        }

        val response = Response.success(LoginEntity(mUserId, mToken))
        return mResponseDelegate.returning(Calls.response(response)).login(loginRequest)
    }

    override fun getUser(serverAccessToken: String?, userid: String): Observable<UserEntity> {
        if (serverAccessToken.isNullOrEmpty()) {
            val json = JSONObject()
            val error = Response.error<Any>(403, ResponseBody
                    .create(MediaType.parse("application/json"), json.toString()))
            return mResponseDelegate.returning(Calls.response(error)).getUser("", userid)
        } else if (userid != mUserId) {
            val json = JSONObject()
            val error = Response.error<Any>(404, ResponseBody
                    .create(MediaType.parse("application/json"), json.toString()))
            return mResponseDelegate.returning(Calls.response(error)).getUser("", userid)
        }

        val avatarFile = File(mMockWebServerCache, userid+"avatar.jpeg")
        val avatarUrl: String? = if (avatarFile.exists()) {
            avatarFile.path
        } else {
            null
        }
        val response = Response.success(UserEntity(mValidCredentials.email, avatarUrl))
        return mResponseDelegate.returning(Calls.response(response)).getUser(serverAccessToken, userid)
    }

    override fun uploadAvatar(serverAccessToken: String?, userid: String, avatarRequest: AvatarRequest): Observable<AvatarEntity> {
        if (serverAccessToken.isNullOrEmpty()) {
            val json = JSONObject()
            val error = Response.error<Any>(403, ResponseBody
                    .create(MediaType.parse("application/json"), json.toString()))
            return mResponseDelegate.returning(Calls.response(error)).uploadAvatar("", userid, avatarRequest)
        } else if (userid != mUserId) {
            val json = JSONObject()
            val error = Response.error<Any>(404, ResponseBody
                    .create(MediaType.parse("application/json"), json.toString()))
            return mResponseDelegate.returning(Calls.response(error)).uploadAvatar("", userid, avatarRequest)
        }

        val decodedString = Base64.decode(avatarRequest.avatar, Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        val avatarFile = File(mMockWebServerCache, userid+"avatar.jpeg")

        if (avatarFile.exists()) {
            avatarFile.delete()
        }

        decodedByte.compressAndClose(avatarFile)

        val response = Response.success(AvatarEntity(avatarFile.path))
        return mResponseDelegate.returning(Calls.response(response)).uploadAvatar(serverAccessToken, userid, avatarRequest)
    }
}

fun Bitmap.compressAndClose(file: File, compressFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG, quality: Int = 100) {
    val outputStream = FileOutputStream(file)
    outputStream.use {
        compress(compressFormat, quality, it)
    }
}