package com.blackbelt.bcgcodechallenge.data

import com.blackbelt.bcgcodechallenge.data.model.*
import io.reactivex.Observable
import retrofit2.http.*
import javax.inject.Inject
import javax.inject.Singleton


interface UserDataService {
    @POST("/sessions/new")
    fun login(@Body loginRequest: LoginRequest): Observable<LoginEntity>

    @GET("/user/{userid}")
    fun getUser(@Header("Authorization") serverAccessToken: String?, @Path("userid") userid: String): Observable<UserEntity>

    @POST("/user/{userid}/avatar")
    fun uploadAvatar(@Header("Authorization") serverAccessToken: String?, @Path("userid") userid: String,
                     @Body avatarRequest: AvatarRequest): Observable<AvatarEntity>
}

interface IUserDataRepository {
    fun login(loginRequest: LoginRequest): Observable<LoginEntity>
    fun getUser(serverAccessToken: String?, userid: String): Observable<UserEntity>
    fun uploadAvatar(token: String?, userid: String, avatarRequest: AvatarRequest): Observable<AvatarEntity>
}

@Singleton
class UserDataRepository @Inject constructor(userDataService: UserDataService) : IUserDataRepository {

    private val mUserDataService = userDataService

    override fun login(loginRequest: LoginRequest): Observable<LoginEntity> = mUserDataService.login(loginRequest)

    override fun getUser(serverAccessToken: String?, userid: String): Observable<UserEntity> =
            mUserDataService.getUser(String.format("Bearer %s", serverAccessToken), userid)

    override fun uploadAvatar(token: String?, userid: String, avatarRequest: AvatarRequest): Observable<AvatarEntity> =
            mUserDataService.uploadAvatar(String.format("Bearer %s", token), userid, avatarRequest)
}