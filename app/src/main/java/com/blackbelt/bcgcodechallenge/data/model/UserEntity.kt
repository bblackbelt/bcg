package com.blackbelt.bcgcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class UserEntity(

        @field:SerializedName("email")
        val email: String? = null,

        @field:SerializedName("avatar_url")
        val avatarUrl: String? = null
)