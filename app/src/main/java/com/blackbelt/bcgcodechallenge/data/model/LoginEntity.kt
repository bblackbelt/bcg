package com.blackbelt.bcgcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class LoginEntity(

        @field:SerializedName("userid")
        val userId: String? = null,

        @field:SerializedName("token")
        val token: String? = null
)