package com.blackbelt.bcgcodechallenge.data.model

data class LoginRequest(val email: String, val password: String)