package com.blackbelt.bcgcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class AvatarEntity(@field:SerializedName("avatar_url")
                        val avatarUrl: String? = null)