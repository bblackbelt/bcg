package com.blackbelt.bcgcodechallenge.data

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

interface IPreferencesRepository {
    fun saveToken(token: String)
    fun getToken(): String

    fun saveUserId(userId: String)
    fun getUserId(): String
}

@Singleton
class PreferencesRepository @Inject constructor(sharedPreferences: SharedPreferences) : IPreferencesRepository {

    private val mSharedPreferences = sharedPreferences

    companion object {
        const val TOKEN_KEY = "TOKEN_KEY"
        const val USER_ID = "USER_ID"
    }

    override fun saveToken(token: String) {
        saveString(TOKEN_KEY, token)
    }

    override fun getToken(): String = getString(TOKEN_KEY)

    override fun saveUserId(userId: String) {
        saveString(USER_ID, userId)
    }

    override fun getUserId(): String = getString(USER_ID)

    private fun saveString(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    private fun getString(key: String) = mSharedPreferences.getString(key, "")
}