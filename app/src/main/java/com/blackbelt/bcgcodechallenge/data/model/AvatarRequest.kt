package com.blackbelt.bcgcodechallenge.data.model

data class AvatarRequest(val avatar: String)